# EducationFrontendApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Install local development environment
1. Follow   
   https://angular.io/guide/setup-local#prerequisites
   https://angular.io/guide/setup-local#install-the-angular-cli
   ...to install nodeJS and the Angular CLI
2. cd to root folder, education-frontend-app
   Run command: npm install
3. To start local server, run command: ng serve
   The server will run at `http://localhost:4200/`


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Run in docker
Run `docker build -t education-frontend-app:v1.0.0 -f ./Dockerfile .` in root dir to build (i.e set prefered version number).
Run `docker run -p 8080:8080 -d education-frontend-app:v1.0.0` to start app

Navigate to `http://localhost:8080/`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
