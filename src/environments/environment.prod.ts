export const environment = {
  production: false,
  // educationApi: "https://education-api-ed-api-develop.test.services.jtech.se",
  educationApi: "https://education-api-test.jobtechdev.se",
  // educationApi: "http://localhost:5000",
  forecastApi: "https://apier.arbetsformedlingen.se/api-yrkesprognos/v1/yrkesprognos",
  scbSalaryStatisticsApi: "https://api.scb.se/OV0104/v1/doris/sv/ssd/START/AM/AM0110/AM0110A/LonYrkeRegion4A"
};
