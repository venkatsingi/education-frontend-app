// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // educationApi: "https://education-api-ed-api-develop.test.services.jtech.se",
  educationApi: "https://education-dev-api.jobtechdev.se",
  // educationApi: "http://localhost:5000",
  forecastApi: "https://apier.arbetsformedlingen.se/api-yrkesprognos/v1/yrkesprognos",
  scbSalaryStatisticsApi: "https://api.scb.se/OV0104/v1/doris/sv/ssd/START/AM/AM0110/AM0110A/LonYrkeRegion4A"

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
