import { Component, OnInit } from '@angular/core';
import {MatchedOccupations, RelatedOccupation} from "../model/matched-occupations.model";
import {MatchOccupationsByEducationTextService} from "../services/match-occupations-by-education-text.service";

@Component({
  selector: 'app-match-occupations-by-education-text-result',
  templateUrl: './match-occupations-by-education-text-result.component.html',
  styleUrls: ['./match-occupations-by-education-text-result.component.scss']
})
export class MatchOccupationsByEducationTextResultComponent implements OnInit {

  constructor(
    public matchOccupationsByEducationtextService: MatchOccupationsByEducationTextService
  ) { }

  ngOnInit(): void {
  }
  getRelatedOccupations(): RelatedOccupation[] | undefined {
    return this._getMatchedOccupations().relatedoccupations;
  }

  matchedOccupations = new MatchedOccupations()

  _getMatchedOccupations(): MatchedOccupations {
    this.matchOccupationsByEducationtextService.matchedOccupationsResult.subscribe((matchedRes: MatchedOccupations) => {
      {
        this.matchedOccupations = matchedRes
      }
    });
    return this.matchedOccupations
  }
}
