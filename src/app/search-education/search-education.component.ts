import { Component, OnInit } from '@angular/core';
import {Observable, of, startWith} from 'rxjs';
import {SearchEducationsQuery} from "../model/search-queries.model";
import {FormControl} from "@angular/forms";
import {SearchEducationsService} from "../services/search-educations.service";
import {SearchparametersService} from "../services/searchparameters.service";
import {NavbarService} from "../services/navbar.service";
import {map} from "rxjs/operators";
import {Searchparameter} from "../model/searchparameter.model";

@Component({
  selector: 'app-search-education',
  templateUrl: './search-education.component.html',
  styleUrls: ['./search-education.component.scss']
})
export class SearchEducationComponent implements OnInit {
  freeTextSearchCtrl: FormControl;
  municipalityCodeFilterCtrl: FormControl;
  educationCodeFilterCtrl: FormControl;
  paceOfStudyPercentageFilterCtrl: FormControl;

  selectedEducationType:string;
  selectedEducationForm:string;
  educationTypes: Array<Searchparameter>;
  educationForms: Array<Searchparameter>;
  municipalities: Array<Searchparameter>;
  filteredMunicipalities: Observable<Searchparameter[]>;

  constructor(
    private educationsService: SearchEducationsService,
    private navbarService: NavbarService,
    private searchparametersService: SearchparametersService
  ) {
    this.freeTextSearchCtrl = new FormControl();

    this.educationCodeFilterCtrl = new FormControl();
    this.paceOfStudyPercentageFilterCtrl = new FormControl();

    this.selectedEducationType = '';
    this.selectedEducationForm = '';
    this.municipalityCodeFilterCtrl = new FormControl();
    this.filteredMunicipalities = new Observable<Searchparameter[]>();
    this.educationTypes = new Array<Searchparameter>();
    this.educationForms = new Array<Searchparameter>();
    this.municipalities = new Array<Searchparameter>();

  }
  ngOnInit(): void {

    this.loadSearchparameters();

    this.filteredMunicipalities = this.municipalityCodeFilterCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );

  }

  displayFnMunicipality(municipality: any): string {
    return municipality && municipality.value ? municipality.value : '';
  }

  private _filter(value: string): Searchparameter[] {
    if (typeof value === "object") {
      return []
    }

    const filterValue = value.toLowerCase();
    // @ts-ignore
    return this.municipalities.filter(option => option.value.toLowerCase().startsWith(filterValue));
  }

  private loadSearchparameters() {
    this.searchparametersService.getEducationTypes().subscribe(items => {
      this.educationTypes = items;
    });

    this.searchparametersService.getEducationForms().subscribe(items => {
      this.educationForms = items;
    });

    this.searchparametersService.getMunicipalities().subscribe(items => {
      this.municipalities = items;
      const nrOfItems = this.municipalities.length;
      console.log('Loaded ' + nrOfItems + ' municipalities');
    });
  }

  search(): void {
    const searchEducationsQuery: SearchEducationsQuery = {
      freeText: '',
      educationType: '',
      educationForm: '',
      municipalityCode: '',
      educationCode:'',
      paceOfStudyPercentage: '',
      limit: 10,
      offset: 0
    }
    this.navbarService.displayEducationDetails = false
    this.navbarService.displayMatchedOccupations = false
    if (this.freeTextSearchCtrl.value) {
      searchEducationsQuery.freeText = this.freeTextSearchCtrl.value;
    }

    if (this.selectedEducationType !== "") {
      searchEducationsQuery.educationType = this.selectedEducationType;
    }

    if (this.selectedEducationForm !== "") {
      searchEducationsQuery.educationForm = this.selectedEducationForm;
    }
    if (this.municipalityCodeFilterCtrl.value) {
      searchEducationsQuery.municipalityCode = this.municipalityCodeFilterCtrl.value.key;
    }


    console.log('searchEducationsQuery:' + JSON.stringify(searchEducationsQuery));

    this.educationsService.searchEducations(searchEducationsQuery, false)
  }

}
