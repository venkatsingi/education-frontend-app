import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import { SearchEducationComponent } from './search-education/search-education.component';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatInputModule} from "@angular/material/input";
import { SearchEducationResultComponent } from './search-education-result/search-education-result.component';
import { SearchEducationDetailsComponent } from './search-education-details/search-education-details.component';
import { SelectOccupationComponent } from './select-occupation/select-occupation.component';
import { SelectOccupationMatchEducationsComponent } from './select-occupation-match-educations/select-occupation-match-educations.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {RouterModule} from "@angular/router";
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatButtonModule} from "@angular/material/button";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatCardModule} from "@angular/material/card";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatSelectModule} from "@angular/material/select";
import {MatTabsModule} from "@angular/material/tabs";
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { SearchEducationMatchOccupationsComponent } from './search-education-match-occupations/search-education-match-occupations.component';
import { MatchEducationsByOccupationComponent } from './match-educations-by-occupation/match-educations-by-occupation.component';
import { MatchEducationsByOccupationResultComponent } from './match-educations-by-occupation-result/match-educations-by-occupation-result.component';
import { MatchEducationsByOccupationDetailsComponent } from './match-educations-by-occupation-details/match-educations-by-occupation-details.component';
import {MatDividerModule} from "@angular/material/divider";
import {NgxGaugeModule} from "ngx-gauge";
import {LinkHttpPipe} from "./pipes/link-http.pipe";
import { MatchOccupationsByEducationTextComponent } from './match-occupations-by-education-text/match-occupations-by-education-text.component';
import { MatchOccupationsByEducationTextResultComponent } from './match-occupations-by-education-text-result/match-occupations-by-education-text-result.component';
import {MatSliderModule} from "@angular/material/slider";

@NgModule(
  {
      declarations: [
          AppComponent,
          SearchEducationComponent,
          SearchEducationResultComponent,
          SearchEducationDetailsComponent,
          SelectOccupationComponent,
          SelectOccupationMatchEducationsComponent,
          SearchEducationMatchOccupationsComponent,
          MatchEducationsByOccupationComponent,
          MatchEducationsByOccupationDetailsComponent,
          MatchEducationsByOccupationResultComponent,
          LinkHttpPipe,
          MatchOccupationsByEducationTextComponent,
          MatchOccupationsByEducationTextResultComponent,
      ],

    imports: [
      BrowserModule,
      BrowserAnimationsModule,
      FormsModule,
      InfiniteScrollModule,
      ReactiveFormsModule,
      HttpClientModule,
      FlexLayoutModule,
      MatIconModule,
      RouterModule,
      MatFormFieldModule,
      MatInputModule,
      MatButtonModule,
      MatButtonToggleModule,
      FontAwesomeModule,
      MatProgressSpinnerModule,
      AppRoutingModule,
      MatAutocompleteModule,
      MatCardModule,
      MatExpansionModule,
      MatSelectModule,
      MatTabsModule,
      NgxChartsModule,
      MatDividerModule,
      NgxGaugeModule,
      MatSliderModule,
    ],
  providers: [{provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}}],
  bootstrap: [AppComponent]
}
)

export class AppModule { }
