import {Component, EventEmitter} from '@angular/core';
import {EducationLight} from "./model/searched-educations.model";
import {SearchEducationsService} from "./services/search-educations.service";
import {NavbarService} from "./services/navbar.service";
import {EducationMatchResultLight} from "./model/matched-educations-by-occupation.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    public navbarService: NavbarService
  ) { }

  title = 'education-frontend-app';
  educationLight: EventEmitter<EducationLight> = new EventEmitter<EducationLight>();

  educationResultLight: EventEmitter<EducationMatchResultLight> = new EventEmitter<EducationMatchResultLight>();

  updateEducationInfo(education: any) {
    this.educationLight.emit(education);
  }

  updateEducationResultInfo(education: any) {
    this.educationResultLight.emit(education);
  }

}
