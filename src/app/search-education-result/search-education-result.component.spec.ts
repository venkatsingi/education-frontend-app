import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEducationResultComponent } from './search-education-result.component';

describe('SearchEducationResultComponent', () => {
  let component: SearchEducationResultComponent;
  let fixture: ComponentFixture<SearchEducationResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchEducationResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEducationResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
