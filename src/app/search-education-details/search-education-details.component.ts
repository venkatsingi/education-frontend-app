import {Component, EventEmitter, Input, OnChanges, OnInit,} from '@angular/core';
import {EducationLight} from "../model/searched-educations.model";
import {EducationEvent, SearchedEducation} from "../model/searched-education.model";
import {NavbarService} from "../services/navbar.service";
import {FetchEducationService} from "../services/fetch-education.service";
import {SearchparametersService} from "../services/searchparameters.service";
import {Searchparameter} from "../model/searchparameter.model";
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-search-education-details',
  templateUrl: './search-education-details.component.html',
  styleUrls: ['./search-education-details.component.scss']
})
export class SearchEducationDetailsComponent implements OnInit, OnChanges {

  @Input() educationInput: EventEmitter<EducationLight> | undefined;

  educationLight: EducationLight | undefined;
  education: SearchedEducation | undefined;
  municipalities: Array<Searchparameter> | undefined;
  /* Number of education providers and events to show in lists: */
  showNumberOfItems: number = 20;

  constructor(
    private fetchEducationService: FetchEducationService,
    private searchparametersService: SearchparametersService,
    public navbarService: NavbarService)
  { }

  ngOnInit(): void {
    this.loadMunicipalities();
  }

  ngOnChanges(): void {
    this.subscribeChanges()
  }

  getTitle(educationLight: EducationLight | undefined): string {
    let title = ""
    if (educationLight) {
      if (educationLight.title) {
        title = title + "Om utbildningen " + this.lowercaseWords(educationLight.title);
      }
      if (educationLight.code) {
        title = title + " (" + educationLight.code +  ")";
      }
      if (title) {
        title = title + ":";
      }
    }
    return title;
  }

  lowercaseWords(words: string | undefined): string{
    if (!words) {
      return "";
    }
    return words.replace(/(?:^|\s)\S/g,(res)=>{ return res.toLowerCase();});
  };

  loadMunicipalities() {
    this.searchparametersService.getMunicipalities().subscribe(items => {
      this.municipalities = items;
      const nrOfItems = this.municipalities.length;
      console.log('Loaded ' + nrOfItems + ' municipalities');
    });
  }

  formatLanguage(inLanguage: string | undefined): string {
    let language: string = '';

    if (inLanguage) {
      if (inLanguage.toLowerCase() == 'swe') {
        return 'svenska';
      }
      if (inLanguage.toLowerCase() == 'eng') {
        return 'engelska';
      }
      return inLanguage;
    }
    else {
      return '';
    }
  }

  formatDate(inDate: string | undefined): string {
    if (!inDate) {
      return "";
    }
    if (inDate.length > 10) {
      return formatDate(inDate,'yyyy-MM-dd', 'en-SE');
    } else {
      return inDate;
    }
  }

  getEventTitle(event: EducationEvent): string {
    let title: string = "";
    if (event) {
      const municipality: string = this.getMunicipality(event.municipalityCode);
      if (municipality) {
        title = title + "Kommun: " + municipality;
      }
      if (event.executionStart) {
        if (title != "") {
          title = title + ", ";
        }
        title = title + "Startdatum: " + this.formatDate(event.executionStart);
      }

      if (title == "") {
        title = title + event.provider;
      }
    }

    return title;
  }

  getMunicipality(municipalityCode: string | undefined): string {
    if (municipalityCode && this.municipalities) {
      for (let municipality of this.municipalities) {
        if (municipality && municipality.key == municipalityCode && municipality.value) {
          return municipality.value;
        }
      }
    }
    return ""
  }

  subscribeChanges() {
    // @ts-ignore
    this.educationInput.subscribe((educationInput) => {
      this.educationLight = educationInput;
      if (!educationInput) {
        this.education = undefined;
      } else if (educationInput.identifier) {
        this.fetchEducationService.fetchEducation(educationInput.identifier).subscribe(
          education => {
            if (!education) {
              this.education = undefined;
            }
            else {
              this.education = education;
            }
          }
        );
      }
    });
  }
}
