export class SearchedEducation {
  id: string | undefined;
  education: Education | undefined;
  events: EducationEvent[] = new Array();
  providers: EducationProvider[] = new Array();
}

export class Education {
  resultIsDegree: boolean | undefined;
  recommendedPriorKnowledge: string | undefined;
  code: string | undefined;
  type: string | undefined; // e.g. kurs or program
  description: string | undefined;
  eligibility: string | undefined;
  title: string | undefined;
  form: string | undefined; // e.g. högskoleutbildning
  level: string | undefined; // e.g. grund
  credits: string | undefined;
}

export class EducationEvent {
  executionStart: string | undefined; // TODO Timestamp?
  executionEnd: string | undefined;
  cancelled: boolean | undefined;
  tuitionFee: string | undefined; // TODO number?
  onlyAsPartOfProgram: boolean | undefined;
  itdistance: boolean | undefined;
  paceOfStudyPercentage: string | undefined;
  url: string | undefined;
  lastApplicationDate: string | undefined;
  languageOfInstruction: string | undefined;
  provider: string | undefined;
  timeOfStudy: string | undefined; // e.g. dag
  municipalityCode: string | undefined; // 4 digits code
}

export class EducationProvider {
  emailAddress: string | undefined;
  phone: string | undefined;
  name: string | undefined;
  contactAddress: Address | undefined;
  visitAddress: Address | undefined;
  url: string | undefined;
}

export class Address {
  town: string | undefined;
  streetAddress: string | undefined;
  postBox: string | undefined;
  postCode: string | undefined;
}


