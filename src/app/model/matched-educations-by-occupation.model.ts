import {MatchEducationsByOccupationQuery} from "./search-queries.model";

export class MatchedEducations {
  totalHits: number | undefined;
  totalPages: number | undefined;
  pageSize: number | undefined;
  page: number | undefined;
  mappedOccupationLabel: string | undefined;
  metadata: EducationMatchResultMetadata | undefined;
  searchQuery: MatchEducationsByOccupationQuery | undefined;
  educations: EducationMatchResultLight[] | undefined;
}

export class EducationMatchResultMetadata {
  occupation_id: string | undefined;
  occupation_label: string | undefined;
  occupation_enriched_ads_count: number | undefined;
  competencies_term_boost: TermBoost[] | undefined;
  competencies_size: number | undefined;
  occupations_term_boost: TermBoost[] | undefined;
  occupations_size: number | undefined;
}

export class TermBoost {
  term: string | undefined;
  boost: number | undefined;
}

export class EducationMatchResultLight {
  id: string | undefined;
  code: string | undefined;
  description: string | undefined;
  title: string | undefined;
  type: string | undefined;
  form: string | undefined;
  providers = new Array();
  municipalityCodes = new Array();
  languagesOfInstruction = new Array();
  onlyAsPartOfPrograms = new Array();
  paceOfStudyPercentages = new Array();
  timeOfStudies = new Array();
  tuitionFees = new Array();
  distance : boolean = false;
}


export class JobtitlesAutocompleteItem {
  jobtitle: string | undefined;
  occupation_id: string | undefined;
  occupation_label: string | undefined;
}

