import {OccupationForecasts} from "./occupation-forecasts.model";
import {OccupationSalaryStatistics} from "./occupation-salary-statistics.model";
import {EducationProvider} from "./searched-education.model";

export class MatchedOccupations {
  hitsTotal: number | undefined;
  hitsReturned: number | undefined;
  relatedoccupations: RelatedOccupation[] = new Array();

  // Metadata that explains what the matching is based on:
  identifiedKeywordsForInput: IdentifiedKeywordsForInput | undefined;
}

export class IdentifiedKeywordsForInput {
  competencies: string[] = new Array();
  occupations: string[] = new Array();
}

export class RelatedOccupation {
  id: string | undefined;
  occupationLabel: string | undefined;
  conceptTaxonomyId: string | undefined;
  legacyAmsTaxonomyId: string | undefined;
  occupationGroup: OccupationGroup | undefined;

  // Additional forecast from other source:
  forecast: OccupationForecasts | undefined;

  // Additional salary data from scb:
  salary: OccupationSalaryStatistics | undefined;
}

export class OccupationGroup {
  occupationGroupLabel: string | undefined;
  conceptTaxonomyId: string | undefined;
  ssyk: string| undefined;
}
