
export class SearchEducationsQuery {
  freeText: string | undefined;
  educationType: string | undefined;
  educationForm: string | undefined;
  municipalityCode: string | undefined;
  educationCode: string | undefined;
  paceOfStudyPercentage: string | undefined;
  limit: number | undefined;
  offset: number | undefined;
}

export class MatchEducationsByOccupationQuery {
  jobtitle: string | undefined;
  educationType: string | undefined;
  educationForm: string | undefined;
  municipalityCode: string | undefined;
  limit: number | undefined;
  offset: number | undefined;
}

export class MatchOccupationsByEducationTextQuery {
  input_text: string | undefined;
  limit: number | undefined;
  offset: number | undefined;
  include_metadata: boolean | undefined;
}
