import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms";
import {MatchOccupationsByEducationTextQuery} from "../model/search-queries.model";
import {MatchOccupationsByEducationTextService} from "../services/match-occupations-by-education-text.service";

@Component({
  selector: 'app-match-occupations-by-education-text',
  templateUrl: './match-occupations-by-education-text.component.html',
  styleUrls: ['./match-occupations-by-education-text.component.scss']
})
export class MatchOccupationsByEducationTextComponent implements OnInit {
  freeTextSearchCtrl: FormControl;
  maxNumberOfMatchesCtrl: FormControl;

  defaultNumberOfMatches: number = 15;

  constructor(
    private matchOccupationsByEducationtextService: MatchOccupationsByEducationTextService,
  ) {
    this.freeTextSearchCtrl = new FormControl();
    this.maxNumberOfMatchesCtrl = new FormControl();
    this.maxNumberOfMatchesCtrl.setValue(this.defaultNumberOfMatches)
  }

  maxNumberOfMatchingLabel(): string {
    if (this.maxNumberOfMatchesCtrl.value) {
      return this.maxNumberOfMatchesCtrl.value;
    }
    else {
      return this.defaultNumberOfMatches.toString();
    }
  }

  ngOnInit(): void {
  }

  formatLabel(value: number) {
    return value;
  }

  updateChange(event: any) {
    this.search();
  }

  search(): void {
    const query: MatchOccupationsByEducationTextQuery = {
      input_text: '',
      limit: this.defaultNumberOfMatches,
      offset: 0,
      include_metadata: false
    }
    if (this.freeTextSearchCtrl.value) {
      query.input_text = this.freeTextSearchCtrl.value;
    }
    else {
      return;
    }
    if (this.maxNumberOfMatchesCtrl.value) {
      query.limit = this.maxNumberOfMatchesCtrl.value;
    }

    console.log('MatchOccupationsByEducationTextQuery:' + JSON.stringify(query));

    this.matchOccupationsByEducationtextService.getMatchedOccupations(query)
  }


}
