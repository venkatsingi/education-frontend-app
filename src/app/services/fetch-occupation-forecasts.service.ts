import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import {Forecast, OccupationForecasts} from "../model/occupation-forecasts.model";

@Injectable({
  providedIn: 'root'
})
export class FetchOccupationForecastsService {

  constructor(private httpClient: HttpClient) {
      this.httpClient
        .get(this.backendUrl).subscribe(forecasts => {
          this.occupationForecastCache = forecasts;
        }
      );
  }

  private backendUrl = environment.forecastApi;

  private occupationForecastCache: any | undefined;

  public fetchOccupationForecasts(ssyk: string): Observable<OccupationForecasts | undefined> {
    // TODO This should not be possible:
    if (!this.occupationForecastCache) {
      return this.httpClient
        .get(this.backendUrl)
        .pipe(map(data => this.handleOccupationForecastsResponse(data, ssyk)));
    }
    else {
      return of(this.occupationForecastCache).pipe(map(data => this.handleOccupationForecastsResponse(data, ssyk)));
    }
  }

  public handleOccupationForecastsResponse(occupationForecastsIn: any, ssyk: string): OccupationForecasts | undefined {
    this.occupationForecastCache = occupationForecastsIn;
    const occupationForecastsOut = new OccupationForecasts()

    const forecasts = occupationForecastsIn['prognoser']

    for (let forecast of forecasts) {
      if (forecast['ssyk'] == ssyk) {

        if (!occupationForecastsOut.ssyk) {
          occupationForecastsOut.ssyk = forecast['ssyk']
        }
        if (!occupationForecastsOut.conceptId) {
          occupationForecastsOut.conceptId = forecast['concept_id']
        }
        if (!occupationForecastsOut.preamble) {
          occupationForecastsOut.preamble = forecast['ingress']
        }
        if (!occupationForecastsOut.paragraph1) {
          occupationForecastsOut.paragraph1 = forecast['stycke1']
        }
        if (!occupationForecastsOut.paragraph2) {
          occupationForecastsOut.paragraph2 = forecast['stycke2']
        }
        if (!occupationForecastsOut.paragraph3) {
          occupationForecastsOut.paragraph3 = forecast['stycke3']
        }

        let forecastOut = new Forecast()
        forecastOut.year = forecast['ar']
        forecastOut.demandValue = forecast['bristvarde']
        forecastOut.geo = forecast['geografi']
        occupationForecastsOut.forecasts.push(forecastOut)
      }
    }

    if (occupationForecastsOut.ssyk) {
      return occupationForecastsOut
    }
    else {
      return undefined;
    }

  }

}
