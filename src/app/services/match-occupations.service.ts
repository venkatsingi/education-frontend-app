import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {
  IdentifiedKeywordsForInput,
  MatchedOccupations,
  OccupationGroup,
  RelatedOccupation
} from "../model/matched-occupations.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MatchOccupationsService {
  private backendUrl = environment.educationApi;

  private _max_result_size = 15;
  private _include_metadata = false;

  constructor(private httpClient: HttpClient) {
  }

  public getMatchedOccupations(educationId: string): Observable<MatchedOccupations> {
    const url = `${this.backendUrl}/occupations/match-by-education?education_id=${educationId}&max_result_size=${this._max_result_size}&include_metadata=${this._include_metadata}`;

    return this.httpClient
      .get(url)
      .pipe(map(data => this.handleMatchedOccupationsResponse(data)));
  }

  public handleMatchedOccupationsResponse(occuationsIn: any): MatchedOccupations {
    const occuationsOut = new MatchedOccupations()
    occuationsOut.hitsTotal = occuationsIn['hits_total']
    occuationsOut.relatedoccupations = occuationsIn['hits_returned']
    occuationsOut.hitsReturned = occuationsIn['hits_returned']

    const occuationsInList = occuationsIn['related_occupations']
    const occuationsOutList: RelatedOccupation[] = [];

    for (let occuationIn of occuationsInList) {
      const occupation = new RelatedOccupation();
      const occupationGroupIn = occuationIn['occupation_group']
      const occupationGroupOut = new OccupationGroup()
      occupationGroupOut.occupationGroupLabel = occupationGroupIn['occupation_group_label']
      occupationGroupOut.conceptTaxonomyId = occupationGroupIn['concept_taxonomy_id']
      occupationGroupOut.ssyk = occupationGroupIn['ssyk']
      occupation.occupationGroup = occupationGroupOut
      occupation.occupationLabel = occuationIn['occupation_label']
      occupation.id = occuationsIn['id']
      occupation.conceptTaxonomyId = occuationIn['concept_taxonomy_id']
      occupation.legacyAmsTaxonomyId = occuationIn['legacy_ams_taxonomy_id']
      occuationsOutList.push(occupation);
    }
    occuationsOut.relatedoccupations = occuationsOutList

    if (occuationsIn['identified_keywords_for_input']) {
      let identifiedKeywords = new IdentifiedKeywordsForInput();
      const identifiedKeywordsIn = occuationsIn['identified_keywords_for_input'];

      if (identifiedKeywordsIn['competencies']) {
        for (let competence of identifiedKeywordsIn['competencies']) {
          identifiedKeywords.competencies.push(competence);
        }
      }
      if (identifiedKeywordsIn['occupations']) {
        for (let occupation of identifiedKeywordsIn['occupations']) {
          identifiedKeywords.occupations.push(occupation);
        }
      }
      occuationsOut.identifiedKeywordsForInput = identifiedKeywords;
    }

    return occuationsOut
  }
}
