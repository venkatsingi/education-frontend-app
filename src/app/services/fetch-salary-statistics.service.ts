import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable, of} from "rxjs";
import {OccupationForecasts} from "../model/occupation-forecasts.model";
import {map} from "rxjs/operators";
import {OccupationSalaryStatistics, SalaryStatistics} from "../model/occupation-salary-statistics.model";

@Injectable({
  providedIn: 'root'
})
export class FetchSalaryStatisticsService {

constructor(private httpClient: HttpClient) {
  this.fetchOccupationSalaryStatistics('');
}

private backendUrl = environment.scbSalaryStatisticsApi;

private occupationSalaryCache: any | undefined;

private static getHeaders(): HttpHeaders {
  let headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Access-Control-Allow-Origin', '*')
  return headers;
}


private yearlySalaryStatistics = {
    "query": [
      {
        "code": "Region",
        "selection": {
          "filter": "item",
          "values": [
            "SE"
          ]
        }
      },
      {
        "code": "Sektor",
        "selection": {
          "filter": "item",
          "values": [
            "0"
          ]
        }
      },
      {
        "code": "Yrke2012",
        "selection": {
          "filter": "item",
          "values": [
            "0110",
            "0210",
            "0310",
            "1111",
            "1112",
            "1113",
            "1120",
            "1211",
            "1212",
            "1221",
            "1222",
            "1230",
            "1241",
            "1242",
            "1251",
            "1252",
            "1291",
            "1292",
            "1311",
            "1312",
            "1321",
            "1322",
            "1331",
            "1332",
            "1341",
            "1342",
            "1351",
            "1352",
            "1361",
            "1362",
            "1371",
            "1372",
            "1380",
            "1411",
            "1412",
            "1421",
            "1422",
            "1491",
            "1492",
            "1511",
            "1512",
            "1521",
            "1522",
            "1531",
            "1532",
            "1540",
            "1591",
            "1592",
            "1611",
            "1612",
            "1711",
            "1712",
            "1721",
            "1722",
            "1731",
            "1732",
            "1741",
            "1742",
            "1791",
            "1792",
            "2111",
            "2112",
            "2113",
            "2114",
            "2121",
            "2122",
            "2131",
            "2132",
            "2133",
            "2134",
            "2135",
            "2141",
            "2142",
            "2143",
            "2144",
            "2145",
            "2146",
            "2149",
            "2161",
            "2162",
            "2163",
            "2164",
            "2171",
            "2172",
            "2173",
            "2179",
            "2181",
            "2182",
            "2183",
            "2211",
            "2212",
            "2213",
            "2219",
            "2221",
            "2222",
            "2223",
            "2224",
            "2225",
            "2226",
            "2227",
            "2228",
            "2231",
            "2232",
            "2233",
            "2234",
            "2235",
            "2239",
            "2241",
            "2242",
            "2250",
            "2260",
            "2271",
            "2272",
            "2273",
            "2281",
            "2282",
            "2283",
            "2284",
            "2289",
            "2311",
            "2312",
            "2313",
            "2314",
            "2319",
            "2320",
            "2330",
            "2341",
            "2342",
            "2343",
            "2351",
            "2352",
            "2359",
            "2411",
            "2412",
            "2413",
            "2414",
            "2415",
            "2419",
            "2421",
            "2422",
            "2423",
            "2431",
            "2432",
            "2511",
            "2512",
            "2513",
            "2514",
            "2515",
            "2516",
            "2519",
            "2611",
            "2612",
            "2613",
            "2614",
            "2615",
            "2619",
            "2621",
            "2622",
            "2623",
            "2641",
            "2642",
            "2643",
            "2651",
            "2652",
            "2653",
            "2654",
            "2655",
            "2661",
            "2662",
            "2663",
            "2669",
            "2671",
            "2672",
            "3111",
            "3112",
            "3113",
            "3114",
            "3115",
            "3116",
            "3117",
            "3119",
            "3121",
            "3122",
            "3151",
            "3152",
            "3153",
            "3154",
            "3155",
            "3211",
            "3212",
            "3213",
            "3214",
            "3215",
            "3230",
            "3240",
            "3250",
            "3311",
            "3312",
            "3313",
            "3314",
            "3321",
            "3322",
            "3323",
            "3324",
            "3331",
            "3332",
            "3333",
            "3334",
            "3335",
            "3339",
            "3341",
            "3342",
            "3343",
            "3351",
            "3352",
            "3353",
            "3354",
            "3355",
            "3359",
            "3360",
            "3411",
            "3412",
            "3421",
            "3422",
            "3423",
            "3424",
            "3431",
            "3432",
            "3433",
            "3439",
            "3441",
            "3449",
            "3451",
            "3452",
            "3511",
            "3512",
            "3513",
            "3514",
            "3515",
            "3521",
            "3522",
            "4111",
            "4112",
            "4113",
            "4114",
            "4115",
            "4116",
            "4117",
            "4119",
            "4211",
            "4212",
            "4221",
            "4222",
            "4223",
            "4224",
            "4225",
            "4226",
            "4321",
            "4322",
            "4323",
            "4410",
            "4420",
            "4430",
            "5111",
            "5112",
            "5113",
            "5120",
            "5131",
            "5132",
            "5141",
            "5142",
            "5143",
            "5144",
            "5149",
            "5151",
            "5152",
            "5161",
            "5169",
            "5221",
            "5222",
            "5223",
            "5224",
            "5225",
            "5226",
            "5227",
            "5230",
            "5241",
            "5242",
            "5311",
            "5312",
            "5321",
            "5322",
            "5323",
            "5324",
            "5325",
            "5326",
            "5330",
            "5341",
            "5342",
            "5343",
            "5349",
            "5350",
            "5411",
            "5412",
            "5413",
            "5414",
            "5419",
            "6111",
            "6112",
            "6113",
            "6121",
            "6122",
            "6129",
            "6130",
            "6210",
            "6221",
            "6222",
            "7111",
            "7112",
            "7113",
            "7114",
            "7115",
            "7116",
            "7119",
            "7121",
            "7122",
            "7123",
            "7124",
            "7125",
            "7126",
            "7131",
            "7132",
            "7133",
            "7134",
            "7211",
            "7212",
            "7213",
            "7214",
            "7215",
            "7221",
            "7222",
            "7223",
            "7224",
            "7231",
            "7232",
            "7233",
            "7311",
            "7312",
            "7319",
            "7321",
            "7322",
            "7323",
            "7411",
            "7412",
            "7413",
            "7420",
            "7521",
            "7522",
            "7523",
            "7531",
            "7532",
            "7533",
            "7534",
            "7611",
            "7612",
            "7613",
            "7619",
            "8111",
            "8112",
            "8113",
            "8114",
            "8115",
            "8116",
            "8121",
            "8122",
            "8129",
            "8131",
            "8132",
            "8141",
            "8142",
            "8143",
            "8151",
            "8159",
            "8161",
            "8162",
            "8163",
            "8169",
            "8171",
            "8172",
            "8173",
            "8174",
            "8181",
            "8189",
            "8191",
            "8192",
            "8193",
            "8199",
            "8211",
            "8212",
            "8213",
            "8214",
            "8219",
            "8311",
            "8312",
            "8321",
            "8329",
            "8331",
            "8332",
            "8341",
            "8342",
            "8343",
            "8344",
            "8350",
            "9111",
            "9119",
            "9120",
            "9210",
            "9310",
            "9320",
            "9331",
            "9332",
            "9411",
            "9412",
            "9413",
            "9520",
            "9610",
            "9621",
            "9622",
            "9629",
            "0001",
            "0002"
          ]
        }
      },
      {
        "code": "Kon",
        "selection": {
          "filter": "item",
          "values": [
            "1+2"
          ]
        }
      },
      {
        "code": "ContentsCode",
        "selection": {
          "filter": "item",
          "values": [
            "000000BW",
            "000000BT"
          ]
        }
      },
      {
        "code": "Tid",
        "selection": {
          "filter": "item",
          "values": [
            "2016",
            "2017",
            "2018",
            "2019",
            "2020"
          ]
        }
      }
    ],
    "response": {
      "format": "json"
    }
  }

  public fetchOccupationSalaryStatistics(ssyk: string): Observable<any | undefined> {
    if (!this.occupationSalaryCache) {
      return this.httpClient
        .post<OccupationSalaryStatistics>(this.backendUrl, JSON.stringify(this.yearlySalaryStatistics), {
          headers: FetchSalaryStatisticsService.getHeaders()
        })
        .pipe(map(data => this.handleOccupationSalaryStatisticsesponse(data, ssyk)));
    }
    else {
      return of(this.occupationSalaryCache).pipe(map(data => this.handleOccupationSalaryStatisticsesponse(data, ssyk)));
    }
  }

  public handleOccupationSalaryStatisticsesponse(occupationSalaryIn: any, ssyk: string | undefined): OccupationSalaryStatistics | undefined {
    // console.log( JSON.stringify(occupationSalaryIn, null, "    ") );
    let salaryStatistics = new OccupationSalaryStatistics();
    salaryStatistics.ssyk = ssyk;

    if (occupationSalaryIn['data'] && occupationSalaryIn['data'].length > 0) {
      this.occupationSalaryCache = occupationSalaryIn;
      for (let salary of occupationSalaryIn['data']) {
          if (salary['key'] && salary['key'].length >= 5) {
            const salaryKeys = salary['key'];
            const salarySsyk = salaryKeys[2];
            if (salarySsyk == ssyk) {
              let occupationSalary = new SalaryStatistics();
              occupationSalary.region = salaryKeys[0];
              occupationSalary.sector = salaryKeys[1];
              occupationSalary.gender = salaryKeys[3];
              occupationSalary.year = salaryKeys[4];

              if (salary['values'] && salary['values'].length >= 2) {
                const salaryValues = salary['values'];
                occupationSalary.monthlySalary = salaryValues[0];
                occupationSalary.numberOfEmployees = salaryValues[1];
              }
              salaryStatistics.salaryStatistics.push(occupationSalary);
            }
          }
      }
    }
    return salaryStatistics;
  }

}
