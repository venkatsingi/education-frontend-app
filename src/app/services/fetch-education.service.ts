import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Education, EducationEvent, EducationProvider, SearchedEducation} from "../model/searched-education.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class FetchEducationService {

  constructor(private httpClient: HttpClient) {
  }

  private backendUrl = environment.educationApi;

  public fetchEducation(educationId: string): Observable<SearchedEducation> {
    return this.httpClient
      .get(this.backendUrl + '/educations/' + educationId)
      .pipe(map(data => this.handleSearchedEducationResponse(data)));
  }

  public handleSearchedEducationResponse(fetchedEducationInput: any): SearchedEducation {
    const educationOut = new SearchedEducation();
    if (fetchedEducationInput) {
      educationOut.id = fetchedEducationInput['id'];

      let education = new Education();
      if (fetchedEducationInput['education']) {
        const educationIn = fetchedEducationInput['education'];
        if (educationIn['title']) {
          for (let educationTitle of educationIn['title']) {
            if (educationTitle['lang'] == 'swe') {
              education.title = educationTitle['content'];
            }
          }
        }

        education.code = educationIn['code'];

        if (educationIn['credits']) {
          const credits = educationIn['credits'];
          let hp = ''
          if (credits['system'] && credits['system']['code']) {
            hp = credits['system']['code'];
          }
          const credit = credits['credits'];
          education.credits = credit + " " + hp;
        }

        if (educationIn['configuration']) {
          education.type = educationIn['configuration']['code'];
        }
        if (educationIn['form']) {
          education.form = educationIn['form']['code'];
        }
        if (educationIn['description']) {
          for (let educationDescription of educationIn['description']) {
            if (educationDescription['lang'] == 'swe') {
              education.description = educationDescription['content'];
            }
          }
        }
        if (educationIn['eligibility'] && educationIn['eligibility']['eligibilityDescription']) {
          for (let eligibilityDescription of educationIn['eligibility']['eligibilityDescription']) {
            if (eligibilityDescription['lang'] == 'swe') {
              education.eligibility = eligibilityDescription['content'];
            }
          }
        }
        if (educationIn['educationLevel']) {
          education.level = educationIn['educationLevel']['code'];
        }

        if (educationIn['recommendedPriorKnowledge']) {
          for (let recommendedPriorKnowledge of educationIn['recommendedPriorKnowledge']) {
            if (recommendedPriorKnowledge['lang'] == 'swe') {
              education.recommendedPriorKnowledge = recommendedPriorKnowledge['content'];
            }
          }
        }
        education.resultIsDegree = educationIn['resultIsDegree'];

        educationOut.education = education;

      }

      if (fetchedEducationInput['events']) {
        for (let eventIn of fetchedEducationInput['events']) {

          let event = new EducationEvent();
          if (eventIn['execution']) {
            const executionIn = eventIn['execution'];
            event.executionStart = executionIn['start'];
            event.executionEnd = executionIn['end'];
          }

          event.itdistance = eventIn['itdistance'];

          if (eventIn['urls']) {
            for (let url of eventIn['urls']) {
              if (url['lang'] == 'swe') {
                event.url = url['content'];
              }
            }
          }

          event.cancelled = eventIn['cancelled'];
          event.languageOfInstruction = eventIn['languageOfInstruction'];

          if (eventIn['application'] && eventIn['application']['last']) {
            event.lastApplicationDate = eventIn['application']['last'];
          }

          if (eventIn['locations']) {
            for (let location of eventIn['locations']) {
              if (location['country'] == 'SE') {
                event.municipalityCode = location['municipalityCode']
              } else {
                console.warn('Swedish location not found')
              }
            }
          }

          if (eventIn['applicationDetails'] && eventIn['applicationDetails']['onlyAsPartOfProgram']) {
            event.onlyAsPartOfProgram = eventIn['applicationDetails']['onlyAsPartOfProgram'];
          }
          event.paceOfStudyPercentage = eventIn['paceOfStudyPercentage'];
          event.provider = eventIn['provider'];
          if (eventIn['timeOfStudy'] && eventIn['timeOfStudy']['code']) {
            event.timeOfStudy = eventIn['timeOfStudy']['code'];
          }
          if (eventIn['tuitionFee'] && eventIn['tuitionFee']['total']) {
            event.tuitionFee = eventIn['tuitionFee']['total'];
          }
          educationOut.events.push(event)
        }

      }

      if (fetchedEducationInput['education_providers']) {
        for (let providerIn of fetchedEducationInput['education_providers']) {
          let educationProvider = new EducationProvider();
          if (providerIn['name']) {
            for (let name of providerIn['name']) {
              if (name['lang'] == 'swe') {
                educationProvider.name = name['content'];
              }
            }
          }
          if (providerIn['urls']) {
            for (let url of providerIn['urls']) {
              if (url['lang'] == 'swe') {
                educationProvider.url = url['content'];
              }
            }
          }
          educationOut.providers.push(educationProvider);
        }
      }
    }
    return educationOut;
  }
}

// "education_providers": [
//   {
//     "identifier": "p.uoh.gu",
//     "expires": "2031-03-17T17:00:06.545+01:00",
//     "responsibleBody": [
//       {
//         "lang": "swe",
//         "content": "Göteborgs universitet"
//       }
//     ],
//     "responsibleBodyType": {},
//     "emailAddress": "servicecenter@gu.se",
//     "phone": [
//       {
//         "number": "031-786 00 00"
//       }
//     ],
//     "name": [
//       {
//         "lang": "swe",
//         "content": "Göteborgs universitet"
//       },
//       {
//         "lang": "eng",
//         "content": "University of Gothenburg"
//       }
//     ],
//     "contactAddress": {
//       "town": "GÖTEBORG",
//       "postBox": 100,
//       "postCode": "405 30"
//     },
//     "visitAddress": {
//       "town": "GÖTEBORG",
//       "streetAddress": "Universitetsplatsen 1",
//       "postCode": "411 24"
//     },
//     "urls": [
//       {
//         "lang": "swe",
//         "content": "https://www.gu.se/"
//       }
//     ]
//   }
// ],

//   "events": [
//   {
//     "identifier": "e.uoh.kau.tamcs.70347.20212",
//     "execution": {
//       "condition": 1,
//       "start": "2021-08-30T00:00:00.000+02:00",
//       "end": "2023-06-11T00:00:00.000+02:00"
//     },
//     "expires": "2025-01-01T00:00:00.000+01:00",
//     "extension": {
//       "stopWeek": 202202,
//       "keywords": [
//         {
//           "lang": "swe",
//           "content": "Software Engineering"
//         },
//         {
//           "lang": "swe",
//           "content": "Security"
//         },
//         {
//           "lang": "swe",
//           "content": "Computer Science"
//         },
//         {
//           "lang": "swe",
//           "content": "Networking"
//         }
//       ],
//       "tuitionFee": {
//         "total": 260000,
//         "content": true,
//         "first": 65000
//       },
//       "itdistance": [
//         false
//       ],
//       "formOfFunding": [
//         "ORD"
//       ],
//       "admissionDetails": {
//         "selectionModel": "KAUURPÅ",
//         "credentialRatingModel": "KAUMERITPÅ",
//         "eligibilityModelSB": "KAUTAMCSE"
//       },
//       "uniqueIdentifier": "c671f21d-5964-11eb-b74a-3d2036ef4aac",
//       "applicationDetails": {
//         "visibleToInternationalApplicants": true,
//         "visibleToSwedishApplicants": true
//       },
//       "id": [
//         "uh_extension_v1"
//       ],
//       "type": "UHEventExtension",
//       "startPeriod": {
//         "periodNumber": 4,
//         "year": 2021,
//         "vacation": "ht",
//         "week": 202135
//       },
//       "eligibilityExemption": true,
//       "department": 1009,
//       "educationEventRef": []
//     },
//     "education": "i.uoh.kau.tamcs.70347.20212",
//     "lastEdited": "2021-03-16T15:03:47.805+01:00",
//     "paceOfStudyPercentage": 100,
//     "urls": [
//       {
//         "lang": "swe",
//         "content": "http://www.kau.se/utbildning/program-och-kurser/program/TAMCS?occasion=70347"
//       },
//       {
//         "lang": "eng",
//         "content": "http://www.kau.se/en/education/programmes-and-courses/programmes/TAMCS?occasion=70347"
//       }
//     ],
//     "application": {
//       "code": 70347,
//       "last": "2021-04-15T00:00:00.000+02:00"
//     },
//     "languageOfInstruction": "eng",
//     "provider": "p.uoh.kau",
//     "timeOfStudy": {
//       "code": "dag",
//       "type": "C_TimeOfStudy"
//     },
//     "locations": [
//       {
//         "country": "SE",
//         "municipalityCode": "1780"
//       }
//     ]
//   }
// ],


// TODO Implement...


// {
//   "id": "i.uoh.kau.tamcs.70347.20212",
//   "education": {
//   "identifier": "i.uoh.kau.tamcs.70347.20212",
//     "resultIsDegree": true,
//     "expires": "2025-01-01T00:00:00.000+01:00",
//     "recommendedPriorKnowledge": [
//     {
//       "lang": "swe",
//       "content": "uh"
//     }
//   ],
//     "code": "TAMCS",
//     "configuration": {
//     "code": "program",
//       "type": "C_Configuration"
//   },
//   "subject": [
//     {
//       "code": "447",
//       "name": "Informationsteknik",
//       "nameEn": "Information Technology",
//       "type": "UOH"
//     },
//     {
//       "code": "540",
//       "name": "Informationsteknik",
//       "nameEn": "Information Technology",
//       "type": "UOH"
//     },
//     {
//       "code": "537",
//       "name": "Informatik",
//       "nameEn": "Information Systems",
//       "type": "UOH"
//     },
//     {
//       "code": "595",
//       "name": "Gjutning",
//       "type": "FHSK"
//     }
//   ],
//     "description": [
//     {
//       "lang": "swe",
//       "content": "Detta program är ett 2-årigt program som bygger på en 3-årig kandidatexamen i datavetenskap eller motsvarande. Målet är att förse studenten med avancerade kunskaper inom datavetenskap och i synnerhet inom datasäkerhet, datakommunikation och trådlösa nätverk.  Fria sökord: Networking Security Software Engineering Computer Science"
//     },
//     {
//       "lang": "eng",
//       "content": "This master programme is a two-year programme which builds on a three-year bachelor degree in Computer Science or equivalent. The objective is to provide you with advanced expertise in Computer Science with a focus on security, computer networks and wireless mesh networks."
//     }
//   ],
//     "eligibility": {
//     "eligibilityDescription": [
//       [
//         {
//           "lang": "swe",
//           "content": "Gymnasiets Engelska kurs B eller motsvarande, kandidatexamen 180 hp samt minst 90 hp inom datavetenskap inklusive följande kurser: Programmeringsteknik 7,5 hp Programutvecklingsmetodik 7,5 hp Operativsystem 7,5 hp Datakommunikation I 7,5 hp samt Matematisk statistik 7,5 hp (eller kurser som motsvarar ovanstående)"
//         },
//         {
//           "lang": "eng",
//           "content": "Upper Secondary English course B or equivalent, and a Bachelor of Science degree of 180 ECTS with at least 90 ECTS in Computer science, or equivalent, including the following:Programming Technique 7.5 ECTS Software Development Methodology 7.5 ECTSOperating Systems 7.5 ECTSComputer Networking I 7.5 ECTSMathematical Statistics 7.5 ECTS(or equivalent courses to the above named)"
//         }
//       ]
//     ]
//   },
//   "lastEdited": "2021-03-16T15:03:47.781+01:00",
//     "title": [
//     {
//       "lang": "swe",
//       "content": "Masterprogram i Datavetenskap"
//     },
//     {
//       "lang": "eng",
//       "content": "Master in Computer Science"
//     }
//   ],
//     "form": {
//     "code": "högskoleutbildning",
//       "type": "C_OrganisationForm"
//   },
//   "isVocational": null,
//     "credits": {
//     "system": {
//       "code": "hp",
//         "type": "C_Credits"
//     },
//     "credits": 120
//   },
//   "educationLevel": {
//     "code": "avancerad",
//       "type": "UH_EducationLevel"
//   }
// },
//   "events": [
//   {
//     "identifier": "e.uoh.kau.tamcs.70347.20212",
//     "execution": {
//       "condition": 1,
//       "start": "2021-08-30T00:00:00.000+02:00",
//       "end": "2023-06-11T00:00:00.000+02:00"
//     },
//     "expires": "2025-01-01T00:00:00.000+01:00",
//     "extension": {
//       "stopWeek": 202202,
//       "keywords": [
//         {
//           "lang": "swe",
//           "content": "Software Engineering"
//         },
//         {
//           "lang": "swe",
//           "content": "Security"
//         },
//         {
//           "lang": "swe",
//           "content": "Computer Science"
//         },
//         {
//           "lang": "swe",
//           "content": "Networking"
//         }
//       ],
//       "tuitionFee": {
//         "total": 260000,
//         "content": true,
//         "first": 65000
//       },
//       "itdistance": [
//         false
//       ],
//       "formOfFunding": [
//         "ORD"
//       ],
//       "admissionDetails": {
//         "selectionModel": "KAUURPÅ",
//         "credentialRatingModel": "KAUMERITPÅ",
//         "eligibilityModelSB": "KAUTAMCSE"
//       },
//       "uniqueIdentifier": "c671f21d-5964-11eb-b74a-3d2036ef4aac",
//       "applicationDetails": {
//         "visibleToInternationalApplicants": true,
//         "visibleToSwedishApplicants": true
//       },
//       "id": [
//         "uh_extension_v1"
//       ],
//       "type": "UHEventExtension",
//       "startPeriod": {
//         "periodNumber": 4,
//         "year": 2021,
//         "vacation": "ht",
//         "week": 202135
//       },
//       "eligibilityExemption": true,
//       "department": 1009,
//       "educationEventRef": []
//     },
//     "education": "i.uoh.kau.tamcs.70347.20212",
//     "lastEdited": "2021-03-16T15:03:47.805+01:00",
//     "paceOfStudyPercentage": 100,
//     "urls": [
//       {
//         "lang": "swe",
//         "content": "http://www.kau.se/utbildning/program-och-kurser/program/TAMCS?occasion=70347"
//       },
//       {
//         "lang": "eng",
//         "content": "http://www.kau.se/en/education/programmes-and-courses/programmes/TAMCS?occasion=70347"
//       }
//     ],
//     "application": {
//       "code": 70347,
//       "last": "2021-04-15T00:00:00.000+02:00"
//     },
//     "languageOfInstruction": "eng",
//     "provider": "p.uoh.kau",
//     "timeOfStudy": {
//       "code": "dag",
//       "type": "C_TimeOfStudy"
//     },
//     "locations": [
//       {
//         "country": "SE",
//         "municipalityCode": "1780"
//       }
//     ]
//   }
// ],
//   "education_providers": [
//   {
//     "identifier": "p.uoh.kau",
//     "expires": "2026-05-12T15:32:05.028+02:00",
//     "responsibleBody": [
//       {
//         "lang": "swe",
//         "content": "Karlstads universitet"
//       }
//     ],
//     "responsibleBodyType": {},
//     "emailAddress": "info@kau.se",
//     "phone": [
//       {
//         "number": "054-700 10 00"
//       }
//     ],
//     "name": [
//       {
//         "lang": "swe",
//         "content": "Karlstads universitet"
//       },
//       {
//         "lang": "eng",
//         "content": "Karlstad University"
//       }
//     ],
//     "contactAddress": {
//       "town": "KARLSTAD",
//       "streetAddress": "Karlstads universitet",
//       "postBox": "",
//       "postCode": "651 88"
//     },
//     "visitAddress": {
//       "town": "KARLSTAD",
//       "streetAddress": "Universitetsgatan 2",
//       "postCode": "656 37"
//     },
//     "urls": [
//       {
//         "lang": "swe",
//         "content": "http://www.kau.se/"
//       }
//     ]
//   }
// ],
//   "text_enrichments_results": {
//   "enriched_candidates": {
//     "occupations": [],
//       "competencies": [
//       "datakommunikation",
//       "datasäkerhet",
//       "datavetenskap",
//       "gjutning",
//       "informatik",
//       "informationsteknik",
//       "kandidatexamen",
//       "masterprogram",
//       "networking",
//       "trådlösa nätverk"
//     ],
//       "traits": [],
//       "geos": []
//   }
// }
// }



