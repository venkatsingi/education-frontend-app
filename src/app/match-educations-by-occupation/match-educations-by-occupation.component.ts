import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from "@angular/forms";
import {MatchEducationsByOccupationService} from "../services/match-educations-by-occupation.service";
import {MatchEducationsByOccupationQuery} from "../model/search-queries.model";
import {Searchparameter} from "../model/searchparameter.model";
import {debounceTime, distinctUntilChanged, filter, Observable, of, startWith, switchMap} from "rxjs";
import {map} from "rxjs/operators";
import {SearchparametersService} from "../services/searchparameters.service";
import {JobtitlesAutocompleteItem} from "../model/matched-educations-by-occupation.model";

@Component({
  selector: 'app-match-educations-by-occupation',
  templateUrl: './match-educations-by-occupation.component.html',
  styleUrls: ['./match-educations-by-occupation.component.scss']
})
export class MatchEducationsByOccupationComponent implements OnInit {
  form: FormGroup = new FormGroup({
    jobtitleSearchCtrl: new FormControl(''),
    municipalityCodeFilterCtrl: new FormControl(''),
  })


  jobtitleSearchCtrl: FormControl;
  municipalityCodeFilterCtrl: FormControl;

  selectedEducationType:string;
  selectedEducationForm:string;
  educationTypes: Array<Searchparameter>;
  educationForms: Array<Searchparameter>;
  municipalities: Array<Searchparameter>;
  filteredMunicipalities: Observable<Searchparameter[]>;
  filteredJobtitles: Observable<JobtitlesAutocompleteItem[]>;

  constructor(
    private matchEducationsService: MatchEducationsByOccupationService,
    private searchparametersService: SearchparametersService,
    private formBuilder: FormBuilder
  ) {



    this.jobtitleSearchCtrl = new FormControl();

    this.selectedEducationType = '';
    this.selectedEducationForm = '';
    this.municipalityCodeFilterCtrl = new FormControl();
    this.filteredMunicipalities = new Observable<Searchparameter[]>();
    this.educationTypes = new Array<Searchparameter>();
    this.educationForms = new Array<Searchparameter>();
    this.municipalities = new Array<Searchparameter>();

    //fetchAutocompleteJobtitles
    this.filteredJobtitles = new Observable<JobtitlesAutocompleteItem[]>()
  }

  ngOnInit(): void {
    this.loadSearchparameters();

    this.filteredMunicipalities = this.municipalityCodeFilterCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );

    this.filteredJobtitles = this.jobtitleSearchCtrl.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      filter((name) => !!name),
      switchMap(name => this.matchEducationsService.fetchAutocompleteJobtitles(name))
    );


  }


  displayFnMunicipality(municipality: any): string {
    return municipality && municipality.value ? municipality.value : '';
  }

  displayFnJobtitle(jobtitle: any): string {
    return jobtitle && jobtitle.jobtitle ? jobtitle.jobtitle : '';
  }


  private _filter(value: string): Searchparameter[] {
    if (typeof value === "object") {
      return []
    }

    const filterValue = value.toLowerCase();
    // @ts-ignore
    return this.municipalities.filter(option => option.value.toLowerCase().startsWith(filterValue));
  }

  private loadSearchparameters() {
    this.searchparametersService.getEducationTypes().subscribe(items => {
      this.educationTypes = items;
    });

    this.searchparametersService.getEducationForms().subscribe(items => {
      this.educationForms = items;
    });

    this.searchparametersService.getMunicipalities().subscribe(items => {
      this.municipalities = items;
      const nrOfItems = this.municipalities.length;
      console.log('Loaded ' + nrOfItems + ' municipalities');
    });
  }

  matchEducations(): void {

    if (this.jobtitleSearchCtrl.invalid) {
      return;
    }
    const matchEducationsByOccupationQuery: MatchEducationsByOccupationQuery = {
      jobtitle: '',
      educationType: '',
      educationForm: '',
      municipalityCode: '',
      limit: 10,
      offset: 0
    }


    // this.navbarService.displayEducationDetails = false
    // this.navbarService.displayMatchedOccupations = false
    if (this.jobtitleSearchCtrl.value) {
      matchEducationsByOccupationQuery.jobtitle = this.jobtitleSearchCtrl.value.jobtitle;
    }

    if (this.selectedEducationType !== "") {
      matchEducationsByOccupationQuery.educationType = this.selectedEducationType;
    }

    if (this.selectedEducationForm !== "") {
      matchEducationsByOccupationQuery.educationForm = this.selectedEducationForm;
    }

    if (this.municipalityCodeFilterCtrl.value) {
      matchEducationsByOccupationQuery.municipalityCode = this.municipalityCodeFilterCtrl.value.key;
    }


    console.log('matchEducationsByOccupationQuery:' + JSON.stringify(matchEducationsByOccupationQuery));

    this.matchEducationsService.matchEducationsByJobtitle(matchEducationsByOccupationQuery, false)
  }


}
