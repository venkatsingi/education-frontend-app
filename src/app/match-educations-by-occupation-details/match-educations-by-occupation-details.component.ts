import {Component, EventEmitter, Input, OnChanges, OnInit} from '@angular/core';
import {EducationMatchResultLight} from "../model/matched-educations-by-occupation.model";
import {EducationEvent, SearchedEducation} from "../model/searched-education.model";
import {FetchEducationService} from "../services/fetch-education.service";
import {EducationLight} from "../model/searched-educations.model";
import {SearchparametersService} from "../services/searchparameters.service";
import {Searchparameter} from "../model/searchparameter.model";
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-match-educations-by-occupation-details',
  templateUrl: './match-educations-by-occupation-details.component.html',
  styleUrls: ['./match-educations-by-occupation-details.component.scss']
})
export class MatchEducationsByOccupationDetailsComponent implements OnInit, OnChanges {


  @Input() educationLightInput: EventEmitter<EducationMatchResultLight> | undefined;

  educationLight: EducationMatchResultLight | undefined;
  education: SearchedEducation | undefined;
  municipalities: Array<Searchparameter> | undefined;
  /* Number of education providers and events to show in lists: */
  showNumberOfItems: number = 20;

  constructor(
    private fetchEducationService: FetchEducationService,
    private searchparametersService: SearchparametersService,
    // public navbarService: NavbarService
  )
  { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.subscribeChanges()
  }

  loadMunicipalities() {
    this.searchparametersService.getMunicipalities().subscribe(items => {
      this.municipalities = items;
      const nrOfItems = this.municipalities.length;
      console.log('Loaded ' + nrOfItems + ' municipalities');
    });
  }

  formatLanguage(inLanguage: string | undefined): string {
    let language: string = '';

    if (inLanguage) {
      if (inLanguage.toLowerCase() == 'swe') {
        return 'svenska';
      }
      if (inLanguage.toLowerCase() == 'eng') {
        return 'engelska';
      }
      return inLanguage;
    }
    else {
      return '';
    }
  }

  formatDate(inDate: string | undefined): string {
    if (!inDate) {
      return "";
    }
    if (inDate.length > 10) {
      return formatDate(inDate,'yyyy-MM-dd', 'en-SE');
    } else {
      return inDate;
    }
  }

  getEventTitle(event: EducationEvent): string {
    let title: string = "";
    if (event) {
      const municipality: string = this.getMunicipality(event.municipalityCode);
      if (municipality) {
        title = title + "Kommun: " + municipality;
      }
      if (event.executionStart) {
        if (title != "") {
          title = title + ", ";
        }
        title = title + "Startdatum: " + this.formatDate(event.executionStart);
      }

      if (title == "") {
        title = title + event.provider;
      }
    }

    return title;
  }

  getMunicipality(municipalityCode: string | undefined): string {
    if (municipalityCode && this.municipalities) {
      for (let municipality of this.municipalities) {
        if (municipality && municipality.key == municipalityCode && municipality.value) {
          return municipality.value;
        }
      }
    }
    return ""
  }

  subscribeChanges() {

    console.log('In details - subscribeChanges')
    // @ts-ignore
    this.educationLightInput.subscribe((educationInput) => {
      // console.log('In this.educationInput.subscribe')
      this.educationLight = educationInput;
      // console.log('this.educationInput.subscribe - this.educationLight: ' + JSON.stringify(this.educationLight))

      if (!educationInput) {
        this.education = undefined;
      } else if (educationInput.id) {
        this.fetchEducationService.fetchEducation(educationInput.id).subscribe(
          education => {
            if (!education) {
              this.education = undefined;
            }
            else {
              this.education = education;
            }
          }
        );
      }
    });
  }
}
