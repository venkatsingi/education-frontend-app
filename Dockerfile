FROM node:14-alpine AS build
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build --prod


FROM nginxinc/nginx-unprivileged
RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist/education-frontend-app /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
